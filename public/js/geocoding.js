/**
 * Created by Ghost_Wanted on 04/02/16.
 */

var geocoding = {

    formLat : null,
    fromLon : null,
    toLat : null,
    toLon : null,
    fromSearch: null,
    toSearch: null,

    init: function(fromLatLng, toLatLng, fromSearch, toSearch){
        this.formLat = fromLatLng[0];
        this.fromLon = fromLatLng[1];
        this.toLat = toLatLng[0];
        this.toLon = toLatLng[1];
        this.fromSearch = fromSearch;
        this.toSearch = toSearch;
        this.autocomplete(this.fromSearch, this.formLat, this.fromLon, markers.marker("<b>My departure</b>", icons.blueIcon));
        this.autocomplete(this.toSearch, this.toLat, this.toLon, markers.marker("<b>My destination</b>", icons.redIcon));
    },

    updateCamera: function() {
        mapView.changeCameraPosition([this.fromLon.val(), this.formLat.val()], [this.toLon.val(), this.toLat.val()], true)
    },

    autocomplete: function(search, lat, lon, marker) {
        this.ajaxRequest = function  ( request, response ) {
            $.ajax({
                url: global.RootURL + "/api/1.0/search?",
                dataType: "json",
                data: {
                    format: "json",
                    q: search.val(),
                    limit: "7"
                },
                success: function( data ) {
                    response( data );
                }
            });
        };

        this.onFocus = function ( event, ui ) {
            search.val( ui.item.name );
            return false;
        };

        this.done = function( event, ui ) {
            search.val( ui.item.name );
            lat.val( ui.item.lat );
            lon.val( ui.item.lon );

            markers.changeMarkerPosition(marker, [ui.item.lon, ui.item.lat]);

            geocoding.updateCamera();

            return false;
        };

        this.render = function( ul, item ) {
            return $( "<li>" )
                .append( "<div class='row'><div class='col-md-1'><img src='" + item.icon + "'/></div>" +
                "<div class='col-md-11'><a><b>" + item.name + "</b></a></div></div>" )
                .appendTo( ul );
        };

        search.autocomplete({
            minLength: 0,
            source: this.ajaxRequest,
            focus: this.onFocus,
            select: this.done
        }).autocomplete( "instance" )._renderItem = this.render;

        search.change(function() {
            if(search.val().length === 0) {
                mapView.map.removeLayer(marker);
                lat.val("");
                lon.val("");
            }
        });

    }

};


$(function() {
    geocoding.init([$( "#fromLat" ), $( "#fromLon" )], [$( "#toLat" ), $( "#toLon" )], $( "#fromSearch" ),$( "#toSearch" ));
});