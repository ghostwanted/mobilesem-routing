/**
 * Created by Ghost_Wanted on 24/02/16.
 */

$(function() {

    var displayOnMap = function displayOnMap(results) {
        if(results.journeys.length === 0) {
            $("#results-body").html('<div class="row"> <div class="col-md-4"></div> <div class="col-md-4">' +
                '<h3>No results <i class="fa fa-frown-o"></i> </h3></div> <div class="col-md-4"></div>');
        } else {
            var color, sections = results.journeys[results.journeys.length-1].sections, duration;
            $("#title").html('<h4>' + sections[0].from.name + ' <i class="fa fa-arrow-circle-right"></i> ' + sections[sections.length - 1].to.name + '</h4>');

            $("#results-panel").remove();

            for (var j = 0; j < results.journeys.length; j++) {
                sections = results.journeys[j].sections;

                $("#results-col").append('<div id="results-panel-' + j + '" class="panel panel-primary"><div class="panel-heading"><div class="row"><div class="col-md-8"><h3 class="panel-title">' +
                    displayTime(sections[0].departure_date_time) + ' <i class="fa fa-arrow-circle-right"></i> ' +
                    displayTime(sections[sections.length - 1].arrival_date_time) + '</h3></div><div class="col-md-1"></div>' +
                    '<div class="col-md-3"> <h3 class="panel-title"><i id="duration-' + j + '" class="fa fa-clock-o"> </i></h3>' +
                    ' </div></div></div><div id="results-body-' + j + '" class="panel-body"></div>');

                $("#results-body-" + j).append('<table id="results-table-' + j + '" class="table table-striped"><tbody></tbody></table>');

                color = "#08" + Math.floor((Math.random() * 4999) + 1000);
                duration = 0;

                for (var i = 0; i < sections.length; i++) {
                    $("#results-table-" + j + " tbody").append(
                        '<tr><td><b>' + displayTime(sections[i].departure_date_time) +
                        ' <i class="fa fa-arrow-right"></i> ' + displayTime(sections[i].arrival_date_time) + '</b></td>' +
                        '<td>' + sections[i].from.name + ' <i class="fa fa-arrow-right"></i> ' + sections[i].to.name + '</td><td><img src="' +
                        sections[i].icon + '" title="' + sections[i].mode + '" width="14" length="14"/></td>' +
                        '<td><button type="button" class="btn-info btn" data-container="body" data-toggle="popover-' + (j + i) +
                        '" data-content="' + sections[i].mode + " : " + sections[i].display_informations.code + ' (' +
                        Math.floor(sections[i].duration / 60) +' min)"></button> </td></tr>'
                    );

                    $('[data-toggle="popover-' + (j + i) + '"]').popover();
                    mapView.addRoute(sections[i].geojson, myStyle = {"color": color, "weight": 6, "opacity": 0.7});

                    if (i < sections.length && i != 0) {
                        markers.addMarker(markers.marker("<b>Stop (" + sections[i].mode + ")</b> : " + sections[i].from.name, icons.greyIcon), sections[i].geojson.coordinates[0]);
                    }
                    duration += sections[i].duration;
                }

                $("#duration-" + j).append(Math.floor(duration / 60) + " min");

                var fromPosition = sections[0].geojson.coordinates[0];
                var toPosition = sections[sections.length - 1].geojson.coordinates[sections[sections.length - 1].geojson.coordinates.length - 1];

                markers.addMarker(markers.marker("<b>My departure (" + sections[0].mode + ")</b> : " + sections[0].from.name, icons.blueIcon), fromPosition);
                markers.addMarker(markers.marker("<b>My destination (" + sections[sections.length - 1].mode + ")</b> : " + sections[sections.length - 1].to.name, icons.redIcon), toPosition);

                mapView.changeCameraPosition(fromPosition, toPosition, true);
            }
        }

    };

    function displayTime(date) {
        var time, hours = new Date(date).getHours(), minutes = new Date(date).getMinutes();

        if(hours/10 >= 1){
            time = hours;
        } else {
            time = "0" + hours;
        }

        if(minutes/10 >= 1){
            time += ":" + minutes;
        } else {
            time += ":0" + minutes;
        }

        return time;
    }

    $.ajax({
        dataType: "json",
        url: global.RootURL + "/api/1.0/route?",
        data: {
            flat : utils.getUrlParameter('flat'),
            flon : utils.getUrlParameter('flon'),
            tlat : utils.getUrlParameter('tlat'),
            tlon : utils.getUrlParameter('tlon'),
            v : utils.getUrlParameter('v')
        },
        success: function(results){

            $("#results-body").empty();
            displayOnMap(results);
        }
    });

    $('[data-toggle="popover"]').popover();

});