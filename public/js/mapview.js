/**
 * Created by Ghost_Wanted on 04/02/16.
 */

var icons = {
    blueIcon: L.icon({
        iconUrl: global.RootURL + '/img/marker-azure.png',
        iconSize: [32, 32],
        iconAnchor:   [16, 32],
        popupAnchor:  [1, -32]
    }),
    redIcon: L.icon({
        iconUrl: global.RootURL  + '/img/marker-pink.png',
        iconSize: [32, 32],
        iconAnchor:   [16, 32],
        popupAnchor:  [1, -32]
    }),
    greyIcon: L.icon({
        iconUrl: global.RootURL  + '/img/marker-grey.png',
        iconSize: [32, 32],
        iconAnchor:   [16, 32],
        popupAnchor:  [1, -32]
    })
};

var markers = {
    marker: function(text, icon) {
        return L.marker([], {
            icon: icon
        }).bindPopup(text).openPopup()
    },

    addMarker: function(marker, coord){
        marker.setLatLng(new L.LatLng(coord[1], coord[0]));
        mapView.map.addLayer(marker);
    },

    changeMarkerPosition: function(marker, coord) {
        mapView.map.removeLayer(marker);

        this.addMarker(marker, coord);
    }
};

var tiles = {

    OpenStreetMap : L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }),

    MapSurfer : L.tileLayer('http://korona.geog.uni-heidelberg.de/tiles/roads/x={x}&y={y}&z={z}', {
        link: 'http://korona.geog.uni-heidelberg.de/tiles/roads/x={x}&y={y}&z={z}',
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="http://www.openstreetmap.org/">OpenStreetMap</a> contributors, powered by <a href="http://mapsurfernet.com/">MapSurfer.NET</a>',
    }),

    Mapbox : L.tileLayer('https://api.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ2hvc3R3YW50ZWQiLCJhIjoiY2lsc2NhYzZzMDA4anV6bHU4YnM4dG5mNiJ9.mfnDh6f0VeOxtlR8Jt4ESQ', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZ2hvc3R3YW50ZWQiLCJhIjoiY2lsc2NhYzZzMDA4anV6bHU4YnM4dG5mNiJ9.mfnDh6f0VeOxtlR8Jt4ESQ'
    })
};

var layers = {

    baseMaps : {
        "OpenStreetMap": tiles.OpenStreetMap,
        "OpenMapSurfer": tiles.MapSurfer,
        "Mapbox" : tiles.Mapbox
    },

    overlayMaps : {
    }
};

var mapView = {
    map: L.map('map'),

    init: function(LatLng, zoom, tile) {
        this.map.setView(LatLng, zoom);
        tile.addTo(this.map);
    },

    changeCameraPosition: function(latLng1, latLng2, animation) {

        this.latLng1 = utils.permuteTable(latLng1);
        this.latLng2 = utils.permuteTable(latLng2);

        this.latLngExist1 = latLng1[0] && latLng1[1];
        this.latLngExist2 = latLng2[0] && latLng2[1];


         if (this.latLngExist1 && this.latLngExist2){
            mapView.map.fitBounds([
                utils.reduceCoordinates(this.latLng1, 0.002),
                utils.reduceCoordinates(this.latLng2, 0.002)
            ]);
         } else if(this.latLngExist1) {
             this.map.panTo(this.latLng1, {animate: animation});
         } else if (this.latLngExist2) {
             this.map.panTo(this.latLng2, {animate: animation});
         }
    },

    addRoute: function(routingLines, myStyle){
        L.geoJson(routingLines, {
            style: myStyle
        }).addTo(this.map);
    }
};

$(function() {

    mapView.init([50.2722, 4.1858], 10, tiles.MapSurfer);

    L.control.layers(layers.baseMaps, layers.overlayMaps).addTo(mapView.map);
});
