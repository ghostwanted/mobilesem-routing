/**
 * Created by Ghost_Wanted on 24/02/16.
 */

$(function() {

    function updateVehicles(tab, element, add) {

        if(add && utils.checkElementExists(tab, element) == false){
            tab.push(element);
        } else if(!add && utils.checkElementExists(tab, element) == true) {
            utils.deleteElement(tab, element);
        }
        $('#vehicles').val(utils.arrayToString(tab));
    }

    $('.vCheck').change(function () {
        var vehicules = $('#vehicles').val();
        var checked = $(this).prop( "checked");

        vehicules = vehicules.split(',').length < 2 ? [vehicules] : vehicules.split(',');
        updateVehicles(vehicules, $(this).val(), checked);
    });


    $('form').submit(function () {
        var fromLat = $.trim($('#fromLat').val());
        var fromLon = $.trim($('#fromLon').val());
        var toLat = $.trim($('#toLat').val());
        var toLon = $.trim($('#toLon').val());
        // Check if empty of not
        if (!fromLat || !fromLon || !toLat || !toLon) {
            $("#alert").show();
            changeColor($("#from-group"), !fromLat || !fromLon);
            changeColor($("#to-group"), !toLat || !toLon);
            return false;
        }
        return true;
    });

    function changeColor(group, alert)
    {
        if(alert) {
            group.removeClass("has-success");
            group.addClass("has-error");
        } else {
            group.removeClass("has-error");
            group.addClass("has-success");
        }
    }

});