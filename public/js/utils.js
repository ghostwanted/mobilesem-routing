/**
 * Created by Ghost_Wanted on 12/02/16.
 */

var utils = {

    getLatLngCenter: function(lat1, lon1, lat2, lon2){

        this.dLon = this.convertToRadians(lon2 - lon1);

        lat1 = this.convertToRadians(lat1);
        lat2 = this.convertToRadians(lat2);
        lon1 = this.convertToRadians(lon1);

        this.Bx = Math.cos(lat2) * Math.cos(this.dLon);
        this.By = Math.cos(lat2) * Math.sin(this.dLon);
        this.lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + this.Bx) * (Math.cos(lat1) + this.Bx) + this.By * this.By));
        this.lon3 = lon1 + Math.atan2(this.By, Math.cos(lat1) + this.Bx);

        return {
            lat : this.convertToDegrees(this.lat3),
            lon : this.convertToDegrees(this.lon3)
        }
    },

    convertToRadians : function (degrees) {
        return degrees * (Math.PI/180);
    },

    convertToDegrees : function (radians) {
        return radians * (180/Math.PI);
    },

    checkElementExists : function (tab, elem) {
        this.result = false;
        for(var i = 0; i < tab.length; i++){
            if(tab[i] === elem){
                this.result = true;
            }
        }

        return this.result;
    },

    deleteElement : function(tab, elem) {
        this.index = -1;

        for(var i = 0; i < tab.length; i++){
            if (tab[i] === elem) {
                this.index = i;
            }
        }

        if(this.index > -1){
            delete tab[this.index];
            return true;
        }
        return false;
    },

    arrayToString : function(tab) {
        this.result = "";

        for(var i = 0; i < tab.length; i++){
            if(tab[i]) {
                this.result += tab[i] + ",";
            }
        }
        this.result = this.result.substring(0, this.result.length - 1);

        return this.result;
    },

    getUrlParameter : function(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },

    permuteTable : function(tab) {
        var aux =[], j =0;
        for(var i = tab.length -1; i >= 0; i--){
            aux[j] = tab[i];
            j++;
        }
        return aux;
    },

    reduceCoordinates : function(coord, x) {
        return [coord[0] - x, coord[1] - x];
        return [coord[0] > 0 ? coord[0] - x : coord[0] + x, coord[1] > 0 ? coord[1] - x : coord[1] + x];
    }
};