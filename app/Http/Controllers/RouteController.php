<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 04/02/16
 * Time: 20:02
 */

namespace App\Http\Controllers;

use App\Model\Router\BicycleSharingRouting;
use App\Model\Router\IRailRouter;
use App\Model\Router\StopsRouter;
use Illuminate\Http\Request;
use App\Model\Router\YOURSRouter;
use Mockery\CountValidator\Exception;


class RouteController extends Controller
{

    private $request;

    public function __construct(Request $request){
        $this->request = $request;
    }

    public function show()
    {
        return response()
             ->view('route');
    }

    public function get()
    {
        $fromLat = $this->request->input('flat');
        $fromLon = $this->request->input('flon');
        $toLat = $this->request->input('tlat');
        $toLon = $this->request->input('tlon');
        $format = $this->request->input('format') ? $this->request->input('format') : 'json';
        $vehicles = explode(',', $this->request->input('v'));

        if($fromLat && $fromLon && $toLat && $toLon && $vehicles) {
            $result = $journeys = array();

            $this->setJourneys($journeys, $vehicles, $fromLat, $fromLon, $toLat, $toLon, $format);

            $result["journeys"] = $this->sortArrayByDate($journeys);

            return $result;
        }
        else {
            return json_encode(array());
        }
    }

    private function setJourneys(&$journeys, $vehicles, $fromLat, $fromLon, $toLat, $toLon, $format)
    {
        $foot = $bicycle = $motorcar = false;
        $train = $bus = $subway = $tramway = false;
        $bicyclesharing = $vtc = $carsharing = $carpool = false;

        foreach($vehicles as $v)
        {
            if(strcmp(strtolower($v), "foot") === 0 || strcmp(strtolower($v), "bicycle") === 0 || strcmp(strtolower($v), "motorcar") === 0) {
                $yours = new YOURSRouter(array("lat" => $fromLat, "lon" => $fromLon), array("lat" => $toLat, "lon" => $toLon));
                $yours->setVehicle(strtolower($v));
                $this->addJourney($journeys, $yours->getJourney());
            } else if(strcmp(strtolower($v), "train") === 0) {
                $train = true;
            } else if(strcmp(strtolower($v), "bus") === 0) {
                $bus = true;
            } else if(strcmp(strtolower($v), "subway") === 0) {
                $subway = true;
            } else if(strcmp(strtolower($v), "tramway") === 0) {
                $tramway = true;
            } else if(strcmp(strtolower($v), "bicyclesharing") === 0) {
                $bsr = new BicycleSharingRouting(array("lat" => $fromLat, "lon" => $fromLon), array("lat" => $toLat, "lon" => $toLon));
                $this->addJourney($journeys, $bsr->getJourney());
            } else if(strcmp(strtolower($v), "uber") === 0) {
                $vtc = true;
            } else if(strcmp(strtolower($v), "carsharing") === 0) {
                $carsharing = true;
            } else if(strcmp(strtolower($v), "carpool") === 0) {
                $carpool = true;
            }
        }
        if($train && $bus && $subway && $tramway)
        {
            $irailRouter = new IRailRouter(array("lat" => $fromLat, "lon" => $fromLon), array("lat" => $toLat, "lon" => $toLon), $format);
            $navitiaRouter = new StopsRouter(array("lat" => $fromLat, "lon" => $fromLon), array("lat" => $toLat, "lon" => $toLon), $format);
            $sncfRouter = new StopsRouter(array("lat" => $fromLat, "lon" => $fromLon), array("lat" => $toLat, "lon" => $toLon), $format, "https://api.sncf.com/v1/", "ba685972-a06f-4c28-8e21-02d7ad00de72");

            if(count($navitiaRouter->getJourney()) > 0)
                $this->addJourney($journeys, $navitiaRouter->getJourney());
            if(count($irailRouter->getJourney()) > 0)
                $this->addJourney($journeys, $irailRouter->getJourney());
            if(count($sncfRouter->getJourney()) > 0)
                $this->addJourney($journeys, $sncfRouter->getJourney());
        }

        return $journeys;
    }

    private function addJourney(array &$journeys, $journey)
    {
        if(!empty($journey)) {
            array_push($journeys, $journey);
        }
    }

    private function sortArrayByDate(array $array)
    {
        $array_in_order = false; $size = count($array);

        while(!$array_in_order)
        {
            $array_in_order = true;
            for($i=0 ; $i < $size-1 ; $i++)
            {
                if ($array[$i]["sections"][count($array[$i]["sections"]) - 1]->arrival_date_time > $array[$i + 1]["sections"][count($array[$i + 1]["sections"]) - 1]->arrival_date_time) {
                    $this->swap($array[$i], $array[$i + 1]);
                    $array_in_order = false;
                }
            }
            $size--;
        }
        return $array;
    }

    private function swap(&$x,&$y) {
        $tmp=$x;
        $x=$y;
        $y=$tmp;
    }

}