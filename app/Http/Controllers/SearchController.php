<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 23/02/16
 * Time: 13:23
 */

namespace App\Http\Controllers;


use App\Model\Searcher\PlacesGeocoder;
use App\Model\Searcher\StopsGeocoder;
use App\Model\Searcher\IRailGeocoder;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $request;

    public function __construct(Request $request){
        $this->request = $request;
    }

    public function get()
    {
        // Get Params
        $q = $this->request->input('q');
        $format = $this->request->input('forrmat');
        $limit = $this->request->input('limit') ? $this->request->input('limit') : 3;
        $type = $this->request->input('type');

        if($q) {
            $pSearcher = new PlacesGeocoder();
            $sncbSearcher = new IRailGeocoder();
            $sncfSearcher = new StopsGeocoder("https://api.sncf.com/v1/","sncf", "ba685972-a06f-4c28-8e21-02d7ad00de72");
            $navitiaBESearcher = new StopsGeocoder("https://api.navitia.io/v1/", "be");
            //$navitiaFRIDFSearcher = new StopsGeocoder("https://api.navitia.io/v1/", "fr-idf");
            $navitiaFRNESearcher = new StopsGeocoder("https://api.navitia.io/v1/", "fr-ne");
            $navitiaFRNWSearcher = new StopsGeocoder("https://api.navitia.io/v1/", "fr-nw");
            $navitiaNLSearcher = new StopsGeocoder("https://api.navitia.io/v1/", "nl");

            // Remplir le resultat à partir des différents API
            $result = array();

            $this->addToFinalResult($q, $pSearcher->forward($q), $result);
            $this->addToFinalResult($q, $sncbSearcher->forward($q), $result);
            $this->addToFinalResult($q, $sncfSearcher->forward($q), $result);
            $this->addToFinalResult($q, $navitiaBESearcher->forward($q), $result);
            //$this->addToFinalResult($q, $navitiaFRIDFSearcher->forward($q), $result);
            $this->addToFinalResult($q, $navitiaFRNESearcher->forward($q), $result);
            $this->addToFinalResult($q, $navitiaFRNWSearcher->forward($q), $result);
            $this->addToFinalResult($q, $navitiaNLSearcher->forward($q), $result);

            // pour le fun
            shuffle($result);

            return json_encode(array_splice($result, 0, $limit));
        } else {
            return "[ ]";
        }
    }

    private function addToFinalResult($q, $tab, &$result)
    {
        $j = $i = 0;
        while($i < count($tab) && $j < 3)
        {
            if(strstr(strtolower($tab[$i]["name"]),strtolower($q),false) || strtolower($tab[$i]["name"]) === strtolower($q)) {
                array_push($result, $tab[$i]);
                $j++;
            }
            $i++;
        }
    }

}