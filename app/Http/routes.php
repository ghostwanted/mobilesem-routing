<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Show Doc
Route::get('/api/1.0/', ['as' => 'doc.show', 'uses' => 'DocController@show']);

// API
Route::get('/api/1.0/route/', ['as' => 'route.get', 'uses' => 'RouteController@get']);
Route::get('/api/1.0/search/', ['as' => 'search.get', 'uses' => 'SearchController@get']);



// Application
Route::get('/', ['as' => 'home.show', 'uses' => 'HomeController@show']);
Route::get('/route/', ['as' => 'route.show', 'uses' => 'RouteController@show']);

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
