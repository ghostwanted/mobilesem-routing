<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 24/02/16
 * Time: 10:07
 */

namespace App\Model\Searcher;


use App\Model\Utils;
use Mockery\CountValidator\Exception;


class PlacesGeocoder extends Geocoder
{
    private $key;
    public function __construct($server = "http://nominatim.openstreetmap.org/search", $format = "json", $key = "")
    {
        parent::setServer($server);
        parent::setFormat($format);
        $this->key = $key;
    }

    public function forward($q, $limit = "3")
    {
        try {
            $URL = parent::getURL(array("q" => $q, "format" => parent::getFormat(), "limit" => $limit, "key" => $this->key));

            $obj = Utils::getObjFromWebService($URL, parent::getFormat());

            return $this->JSONFormatter($obj);
        } catch (Exception $e){
            return array();
        }

    }

    public function reverse(array $coord)
    {
        try {
            $URL = parent::getURL(array("q" => $coord["lat"] . "+" . $coord["lon"], "format" => parent::getFormat(), "key" => $this->key));

            return Utils::getObjFromWebService($URL, parent::getFormat());
        } catch (Exception $e){
            return array();
        }
    }

    protected function JSONFormatter($params, $icon = "/img/place.png")
    {
        $objs = array();

        foreach($params as $p){
            // Limiter les recherches en Europe
            if($p->importance > 0.2 && $p->lat < 71.1 && $p->lat > 37.3 && $p->lon > -10 && $p->lon < 28) {
                array_push($objs, parent::objToArray($p->place_id, $p->display_name, $p->type, $p->lat, $p->lon, $icon));
            }
        }

        return $objs;
    }
}