<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 24/02/16
 * Time: 09:59
 */

namespace App\Model\Searcher;


use App\Model\Utils;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;

abstract class Geocoder extends Model
{
    private $server, $format;


    protected function setServer($server)
    {
        $this->server = $server;
    }

    protected function getServer()
    {
        return $this->server;
    }

    public function getFormat()
    {
        return $this->format;
    }

    public function setFormat($format)
    {
        $this->format = $format;
    }

    protected function getURL($params, $subdomain = "")
    {
        return Utils::generateURL($this->server, $params, $subdomain);
    }

    protected function objToArray($id, $name, $type, $lat, $lon, $icon)
    {
        $obj = array();
        $obj["id"] = $id;
        $obj["name"] = $name;
        $obj["type"] = $type;
        $obj["lat"] = $lat;
        $obj["lon"] = $lon;
        $obj["icon"] = URL::to('/') .$icon;
        return $obj;
    }

    protected abstract function forward($q);

    protected abstract function reverse(array $coord);

    protected abstract function JSONFormatter($params, $icon);

}