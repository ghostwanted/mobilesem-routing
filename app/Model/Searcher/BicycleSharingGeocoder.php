<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 11/04/16
 * Time: 14:50
 */

namespace App\Model\Searcher;

class BicycleSharingGeocoder extends Geocoder
{
    public function __construct($server = "http://localhost/mobilesem/public/json/bicycle-sharing.json", $format = "json")
    {
        parent::setServer($server);
        parent::setFormat($format);
    }
    
    public function reverse(array $coord)
    {
        $stations = json_decode(file_get_contents(parent::getServer()));

        foreach($stations->{"bicycle-sharing"} as $s)
        {
            if(number_format($s->latitude, 2)  ==  number_format($coord["lat"], 2) &&
                number_format($s->longitude, 2)  ==  number_format($coord["lon"], 2)) {
                return $s;
            }
        }
        return false;
    }

    protected function forward($q)
    {
        // TODO: Implement forward() method.
    }

    protected function JSONFormatter($params, $icon)
    {
        // TODO: Implement JSONFormatter() method.
    }
}