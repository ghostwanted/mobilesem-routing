<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 25/02/16
 * Time: 09:58
 */

namespace App\Model\Searcher;

use App\Model\Utils;


class IRailGeocoder extends Geocoder
{
    public function __construct($server = "https://irail.be/stations/NMBS", $format = "json")
    {
        parent::setServer($server);
        parent::setFormat($format);
    }

    public function forward($q)
    {
        try {

            $URL = parent::getURL(array("q" => $q, "format" => parent::getFormat()));

            $obj = Utils::getObjFromWebService($URL, parent::getFormat());

            return $this->JSONFormatter($obj->{"@graph"});

        } catch (\Exception $e)
        {
            return array();
        }
    }

    public function reverse(array $coord)
    {
        $stations = Utils::getObjFromWebService(parent::getServer());

        foreach($stations->{"@graph"} as $s)
        {
            if(number_format($s->latitude, 2)  ==  number_format($coord["lat"], 2) &&
                number_format($s->longitude, 2)  ==  number_format($coord["lon"], 2)) {
                return $s;
            }
        }
        return false;
    }

    public function JSONFormatter($params, $icon = "/img/sncb.png")
    {
        $objs = array();

        foreach($params as $p){
            if(isset($p->name)) {
                array_push($objs, parent::objToArray($p->{"@id"}, $p->name, "SNCB", $p->latitude, $p->longitude, $icon));
            }
        }

        return $objs;
    }
}