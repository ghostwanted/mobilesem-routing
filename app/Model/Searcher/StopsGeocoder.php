<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 29/02/16
 * Time: 14:49
 */

namespace App\Model\Searcher;

use App\Model\Utils;

// SNCF, STIB, TEC, De Lijn...
class StopsGeocoder extends Geocoder
{
    private $username, $password, $region;

    public function __construct($server = "https://api.navitia.io/v1/", $region="be", $username = "9a147cf1-e488-4568-b1fc-533aaace7f1c", $password = "", $format = "json")
    {
        parent::setServer($server);
        parent::setFormat($format);
        $this->username = $username;
        $this->password = $password;
        $this->region = $region;
    }

    public function forward($q)
    {
        try{
            $URL = parent::getURL(array("q" => $q, "format" => parent::getFormat()), "coverage/" . $this->region . "/places/");

            $obj = Utils::getObjFromWebService($URL, parent::getFormat(), $this->username, $this->password);

            return $this->JSONFormatter($obj->{"places"});
        } catch (\Exception $e)
        {
            return array();
        }
    }

    public function reverse(array $coord)
    {
        try {
            $clat = number_format($coord["lat"], 2);
            $clon = number_format($coord["lon"], 2);

            $URL = parent::getURL(array("format" => parent::getFormat()), "coord/" . $clon . ';' .$clat);

            $station = Utils::getObjFromWebService($URL, "json", $this->username, $this->password);

            $stat = number_format($station->address->coord->lat, 2);
            $slon = number_format($station->address->coord->lon, 2);

            if ($clat === $stat && $clon === $slon) {
                return $station->address;
            }

            return false;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function JSONFormatter($params, $icon = "/img/flag.png")
    {
        $objs = array();

        foreach($params as $p) {

            if(strcmp($p->embedded_type, "stop_area") == 0) {

                if(strpos($p->stop_area->id,"STE") || strpos($p->stop_area->id,"OCE")) {
                    array_push($objs, parent::objToArray($p->stop_area->id, $p->stop_area->name, "SNCF", $p->stop_area->coord->lat, $p->stop_area->coord->lon, "/img/sncf.png"));
                }
                else if(strpos($p->stop_area->id,"OWY")) {
                    array_push($objs, parent::objToArray($p->stop_area->id, $p->stop_area->name, "TEC", $p->stop_area->coord->lat, $p->stop_area->coord->lon, "/img/tec.png"));
                }
                else if(strpos($p->stop_area->id,"OFL")) {
                    array_push($objs, parent::objToArray($p->stop_area->id, $p->stop_area->name, "De Lijn ", $p->stop_area->coord->lat, $p->stop_area->coord->lon, "/img/delijn.png"));
                }
                else if(strpos($p->stop_area->id,"OBG")){
                    array_push($objs, parent::objToArray($p->stop_area->id, $p->stop_area->name, "STIB", $p->stop_area->coord->lat, $p->stop_area->coord->lon, "/img/stib.png"));
                }
                else if(strpos($p->stop_area->id,"OST")){
                    array_push($objs, parent::objToArray($p->stop_area->id, $p->stop_area->name, "CTS", $p->stop_area->coord->lat, $p->stop_area->coord->lon, "/img/cts.png"));
                }
                else if(strpos($p->stop_area->id,"OMZ")){
                    array_push($objs, parent::objToArray($p->stop_area->id, $p->stop_area->name, "LE MET'", $p->stop_area->coord->lat, $p->stop_area->coord->lon, "/img/lemet.png"));
                }
                else if(strpos($p->stop_area->id,"ONY")){
                    array_push($objs, parent::objToArray($p->stop_area->id, $p->stop_area->name, "STAN'", $p->stop_area->coord->lat, $p->stop_area->coord->lon, "/img/stan.png"));
                }
                else {
                    array_push($objs, parent::objToArray($p->stop_area->id, $p->stop_area->name, "Stop_area", $p->stop_area->coord->lat, $p->stop_area->coord->lon, $icon));
                }

            }
        }

        return $objs;
    }
}