<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 24/02/16
 * Time: 11:19
 */

namespace App\Model;


use DOMDocument;

class Utils
{
    public static function authentication($username, $password)
    {
        return $context = stream_context_create(array(
            'http' => array(
                'header'  => "Authorization: Basic " . base64_encode("$username:$password")
            )
        ));
    }

    public static function generateURL($server, $params, $subdomain = "")
    {
        $URL = ($subdomain ? ($server . (substr($server, -1) === "/" ? "" : "/") . $subdomain) : $server) . "?";

        foreach ($params as $key => $value) {
            if($value) {
                $URL .= $key . "=" . $value . "&";
            }
        }

        return substr($URL, -1) === "&" ? substr($URL,0,-1) : $URL;
    }

    public static function getObjFromWebService($URL, $format = "json", $username = "", $password = "")
    {
        $ch = curl_init( str_replace(" " , "+", $URL) );

        $options = array(CURLOPT_RETURNTRANSFER => true, CURLOPT_USERPWD => $username . ":" . $password,
            CURLOPT_HTTPHEADER => array('Content-type: application/'. $format));

        curl_setopt_array( $ch, $options );

        if($format == "xml") {
            return Utils::xml_encode(curl_exec($ch));
        } else  {
            return json_decode(curl_exec($ch), false);
        }

    }

    public static function xml_encode($mixed, $domElement=null, $DOMDocument=null) {
        if (is_null($DOMDocument)) {
            $DOMDocument =new DOMDocument;
            $DOMDocument->formatOutput = true;
            Utils::xml_encode($mixed, $DOMDocument, $DOMDocument);
            echo $DOMDocument->saveXML();
        }
        else {
            // To cope with embedded objects
            if (is_object($mixed)) {
                $mixed = get_object_vars($mixed);
            }
            if (is_array($mixed)) {
                foreach ($mixed as $index => $mixedElement) {
                    if (is_int($index)) {
                        if ($index === 0) {
                            $node = $domElement;
                        }
                        else {
                            $node = $DOMDocument->createElement($domElement->tagName);
                            $domElement->parentNode->appendChild($node);
                        }
                    }
                    else {
                        $plural = $DOMDocument->createElement($index);
                        $domElement->appendChild($plural);
                        $node = $plural;
                        if (!(rtrim($index, 's') === $index)) {
                            $singular = $DOMDocument->createElement(rtrim($index, 's'));
                            $plural->appendChild($singular);
                            $node = $singular;
                        }
                    }

                    xml_encode($mixedElement, $node, $DOMDocument);
                }
            }
            else {
                $mixed = is_bool($mixed) ? ($mixed ? 'true' : 'false') : $mixed;
                $domElement->appendChild($DOMDocument->createTextNode($mixed));
            }
        }
    }

}