<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 24/02/16
 * Time: 09:50
 */

namespace App\Model\Router;


use App\Model\Utils;
use Illuminate\Database\Eloquent\Model;

abstract class Router extends Model
{
    private $server, $from = array(), $to = array(), $format;

    protected function getURL($params, $subdomain = "")
    {
        return Utils::generateURL($this->server, $params, $subdomain);
    }

    protected function setServer($server)
    {
        $this->server = $server;
    }

    protected function getServer()
    {
        return $this->server;
    }

    protected function getFrom()
    {
        return $this->from;
    }

    protected function setFrom($from)
    {
        $this->from = $from;
    }

    protected function getTo()
    {
        return $this->to;
    }

    protected function setTo($to)
    {
        $this->to = $to;
    }

    protected function getFormat()
    {
        return $this->format;
    }

    protected function setFormat($format)
    {
        $this->format = $format;
    }

    protected abstract function getJSON($obj);

    public abstract function getJourney();

    protected function createGeoJSON($coordinates)
    {
        $geojson = new \stdClass();
        $geojson->type = "LineString";
        $geojson->coordinates = $coordinates;
        return $geojson;
    }

}