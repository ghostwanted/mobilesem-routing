<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 31/03/16
 * Time: 11:50
 */

namespace App\Model\Router;

use App\Model\Searcher\BicycleSharingGeocoder;


class BicycleSharingRouting extends Router
{

    public function __construct($from, $to, $format = "json", $server = "http://localhost/mobilesem/public/json/bicycle-sharing.json")
    {
        parent::setFrom($from);
        parent::setTo($to);
        parent::setServer($server);
        parent::setFormat($format);
    }

    protected function getJSON($obj)
    {
        $bsg = new BicycleSharingGeocoder();
        $station = $bsg->reverse(parent::getFrom());

        if($station) {
            $yours = new YOURSRouter(parent::getFrom(), parent::getTo(), "bicycle");

            $section = $yours->getJourney();
            $section["sections"][0]->display_informations->code = $station->type;
            $section["sections"][0]->icon = "img/icons/bicycle-sharing.png";
            $section["sections"][0]->mode = "bicycle-sharing";
            return $section;
        }
        else return array();
    }

    public function getJourney()
    {
        try {
            return $this->getJSON(json_decode(file_get_contents(parent::getServer())));
        } catch(\Exception $e) {
            return array();
        }
    }
}