<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 23/02/16
 * Time: 10:14
 */

namespace App\Model\Router;

use App\Model\Searcher\IRailGeocoder;
use App\Model\Utils;
use Faker\Provider\tr_TR\DateTime;

class IRailRouter extends Router
{

    public function __construct($from, $to, $format = "json", $server = "http://api.irail.be")
    {
        parent::setFrom($from);
        parent::setTo($to);
        parent::setServer($server);
        parent::setFormat($format);
    }

    public function getJourney()
    {
         try {
            $sncbSearcher = new IRailGeocoder();
            $fromStation = $sncbSearcher->reverse(parent::getFrom());
            $toStation = $sncbSearcher->reverse(parent::getTo());

            if($fromStation && $toStation) {

               $url = parent::getURL(array("format" => parent::getFormat(), "from" => $this->IDFormatter($fromStation->{"@id"}),
                    "to" => $this->IDFormatter($toStation->{"@id"})), "connections/");

                return $this->getJSON(Utils::getObjFromWebService($url, parent::getFormat()));
            } else {
                return array();
            }
        } catch(\Exception $e) {
            return array();
        }
    }

    private function IDFormatter($id)
    {
        $idArray = explode("/", $id);
        return trim("BE.NMBS." . $idArray[ count($idArray) - 1]);
    }

    public function getJSON($obj)
    {
        $journeys = $sections = array();
        $departure = null;

        if(isset($obj->connection)) {
            $c = $obj->connection[0];
            //foreach ($obj->connection as $c) {
                $from = new \stdClass();
                $to = new \stdClass();
                $coordinates = array();

                // FROM
                $this->addStopPoint($from, $c->departure->station, $coordinates, [floatval($c->departure->stationinfo->locationX), floatval($c->departure->stationinfo->locationY)]);
                $departure = $c->departure->time;

                if (isset($c->vias)) {
                    $vias = $c->vias->via;
                    for ($i = 0; $i < count($vias); $i++) {
                        if ($i > 0) {
                            $this->addStopPoint($from, $vias[$i - 1]->station, $coordinates, [floatval($vias[$i - 1]->stationinfo->locationX), floatval($vias[$i - 1]->stationinfo->locationY)]);
                            $departure = $vias[$i - 1]->departure->time;
                        }

                        $this->addStopPoint($to, $vias[$i]->station, $coordinates, [floatval($vias[$i]->stationinfo->locationX), floatval($vias[$i]->stationinfo->locationY)]);

                        // SECTION
                        $this->addToSection($sections, $from, $to, $departure, $vias[$i]->arrival->time, parent::createGeoJSON($coordinates), explode('.', $vias[$i]->vehicle)[2]);

                        $from = new \stdClass();
                        $to = new \stdClass();
                        $coordinates = array();
                    }

                    $this->addStopPoint($from, $vias[count($vias) - 1]->station, $coordinates, [floatval($vias[count($vias) - 1]->stationinfo->locationX), floatval($vias[count($vias) - 1]->stationinfo->locationY)]);
                    $departure = $vias[count($vias) - 1]->departure->time;
                }

                // TO
                $this->addStopPoint($to, $c->arrival->station, $coordinates, [floatval($c->arrival->stationinfo->locationX), floatval($c->arrival->stationinfo->locationY)]);

                // SECTION
                $this->addToSection($sections, $from, $to, $departure, $c->arrival->time, parent::createGeoJSON($coordinates), explode('.', $c->arrival->vehicle)[2]);

                array_push($journeys, array("sections" => $sections));

            //}
        }

        return $journeys[0];
        //return json_encode(array("journeys" => $journeys));
    }

    private function addStopPoint(&$stop, $name, &$coordinates, $coord)
    {
        $stop->embedded_type = "stop_point";
        $stop->name = $name;
        array_push($coordinates, $coord);
    }

    private function addToSection(&$sections, $from, $to, $departure, $arrival, $geojson, $code)
    {
        $section = new \stdClass();
        $display_informations = new \stdClass();

        $display_informations->code = $code;

        $section->from = $from;
        $section->arrival_date_time = date('c', $arrival);
        $section->departure_date_time = date('c', $departure);
        $section->display_informations = $display_informations;
        $section->to = $to;
        $section->geojson = $geojson;
        $section->duration = strtotime($section->arrival_date_time) - strtotime($section->departure_date_time);

        $section->type = "public_transport";
        $section->icon = 'img/icons/train.png';
        $section->mode = 'Train';
        array_push($sections, $section);
    }

}