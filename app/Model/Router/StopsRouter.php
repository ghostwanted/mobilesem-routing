<?php
/**
 * Created by PhpStorm.
 * User: Ghost_Wanted
 * Date: 07/03/16
 * Time: 12:05
 */

namespace App\Model\Router;


use App\Model\Searcher\StopsGeocoder;
use App\Model\Utils;

class StopsRouter extends Router
{
    private $username, $password;

    public function __construct($from, $to, $format = "json", $server = "https://api.navitia.io/v1", $username = "9a147cf1-e488-4568-b1fc-533aaace7f1c", $password = "")
    {
        parent::setFrom($from);
        parent::setTo($to);
        parent::setServer($server);
        parent::setFormat($format);
        $this->username = $username;
        $this->password = $password;
    }

    public function getJourney()
    {
        try {
            $stopsGeocoder = new StopsGeocoder();
            $fromStation = $stopsGeocoder->reverse(parent::getFrom());
            $toStation = $stopsGeocoder->reverse(parent::getTo());

            if ($fromStation && $toStation) {

                $fromStationID = strstr(strtolower(parent::getServer()),"sncf",false) ? $fromStation->administrative_regions[0]->id . 'extern' : $fromStation->administrative_regions[0]->id;
                $toStationID = strstr(strtolower(parent::getServer()),"sncf",false) ? $toStation->administrative_regions[0]->id . 'extern' : $toStation->administrative_regions[0]->id;

                date_default_timezone_set('Europe/Brussels');
                $url = parent::getURL(array("datetime" => explode("+", date("c"))[0], "format" => parent::getFormat(), "from" => $fromStationID, "to" => $toStationID), "journeys");

                return $this->getJSON(Utils::getObjFromWebService($url, parent::getFormat(), $this->username, $this->password));
            } else {
                return array();
            }
        } catch(\Exception $e) {
            return array();
        }
    }

    public function getJSON($obj)
    {
        $journeys = $sections = array();

        if(isset($obj->journeys)) {
            //foreach ($obj->journeys as $j) {

                $j = $obj->journeys[0];
                // Search the faster journey
                /*foreach ($obj->journeys as $journey)
                {
                    if($journey->type === "fastest"){
                        $j = $journey;
                    }
                }*/

                foreach ($j->sections as $s) {
                    if ($s->type !== "waiting" && $s->type !== "stay_in" && $s->type !== "crow_fly") {
                        $section = new \stdClass();
                        $from = new \stdClass();
                        $to = new \stdClass();
                        $display_informations = new \stdClass();

                        // FROM
                        $from->embedded_type = $s->from->embedded_type;
                        if ($s->from->embedded_type == "stop_point") {
                            $from->name = $s->from->stop_point->name;
                        } else if ($s->from->embedded_type == "administrative_region") {
                            $from->name = $s->from->administrative_region->name;
                        }

                        // TO
                        $to->embedded_type = $s->to->embedded_type;
                        if ($s->to->embedded_type == "stop_point") {
                            $to->name = $s->to->stop_point->name;
                        } else if ($s->to->embedded_type == "administrative_region") {
                            $to->name = $s->to->administrative_region->name;
                        }

                        // Display informations
                        $display_informations->code = isset($s->display_informations->code) ? $s->display_informations->code : "";


                        // SECTION
                        $section->from = $from;
                        $section->arrival_date_time = date_format(date_create($s->arrival_date_time), 'c');
                        $section->departure_date_time = date_format(date_create($s->departure_date_time), 'c');
                        $section->display_informations = $display_informations;
                        $section->to = $to;
                        if(isset($s->geojson))
                        {
                            $section->geojson = $s->geojson;
                        } else {
                            $coordinates = array();
                            array_push($coordinates, [$section->from->administrative_region->coord->lon, $section->from->administrative_region->coord->lat]);
                            array_push($coordinates, [$section->to->administrative_region->coord->lon, $section->to->administrative_region->coord->lat]);
                            $section->geojson = parent::createGeoJSON($coordinates);
                        }
                        $section->duration = $s->duration;
                        $section->type = $s->type;

                        $this->setIcon($s, $section);

                        array_push($sections, $section);
                    }
                }
                array_push($journeys, array("sections" => $sections));
            //}

        }
        return $journeys[0];
        //return json_encode(array("journeys" => $journeys));
    }

    private function setIcon($s, &$section)
    {
        if ($s->type == "public_transport" || $s->type == "on_demand_transport") {
            $physical_mode = strtolower($s->display_informations->physical_mode);
            if (strstr($physical_mode, 'bus')) {
                $section->icon = 'img/icons/bus.png';
                $section->mode = 'Bus';
            } else if(strstr($physical_mode, 'tramway')) {
                $section->icon = 'img/icons/tramway.png';
                $section->mode = 'Tramway';
            } else if(strstr($physical_mode, 'train') || strstr($physical_mode, 'ter')) {
                $section->icon = 'img/icons/train.png';
                $section->mode = 'Train';
            }
            else {
                return null;
            }
        } else if($s->type == "transfer"){
            if($s->transfer_type == 'walking') {
                $section->icon = 'img/icons/walking.png';
                $section->mode = 'walking';
            }
        } else if(isset($s->mode)){
            if($s->mode === "walking") {
                $section->icon = 'img/icons/walking.png';
                $section->mode = 'walking';
            } else {
                $section->icon = '';
                $section->mode = $s->mode;
            }
        } else {
            $section->icon = '';
            $section->mode = '';
        }
    }
}