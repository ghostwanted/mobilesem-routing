<?php

namespace App\Model\Router;


use App\Model\Searcher\PlacesGeocoder;
use App\Model\Utils;
use Faker\Provider\zh_TW\DateTime;

class YOURSRouter extends Router
{
    private $vehicle;


    public function __construct($from, $to, $vehicle = "motorcar", $format = "geojson", $server = "http://www.yournavigation.org/api/1.0")
    {
        parent::setServer($server);
        parent::setFrom($from);
        parent::setTo($to);
        parent::setFormat($format);
        $this->vehicle = $vehicle;
    }

    public function getJourney()
    {
        try {
            $URL = parent::getURL(array("format" => parent::getFormat(), "v" => $this->vehicle,
                "flat" => parent::getFrom()["lat"], "flon" => parent::getFrom()["lon"],
                "tlat" => parent::getTo()["lat"], "tlon" => parent::getTo()["lon"],
                "instructions" => "1"), "gosmore.php");

            return $this->getJSON(Utils::getObjFromWebService($URL));
        } catch(\Exception $e) {
            return array();
        }
    }

    public function getJSON($obj)
    {
        $journeys = $sections = array();

        $section = new \stdClass();
        $from = new \stdClass();
        $to = new \stdClass();
        $geojson = new \stdClass();
        $display_informations = new \stdClass();

        $pSearcher = new PlacesGeocoder("http://api.opencagedata.com/geocode/v1/", "json", "9677b95220256e324efe9ad6c8827fab");
        $fromPlace = $pSearcher->reverse(array("lat" => $obj->coordinates[0][1], "lon" => $obj->coordinates[0][0]))->results[0]->components;
        $toPlace = $pSearcher->reverse(array("lat" => $obj->coordinates[count($obj->coordinates) - 1][1], "lon" => $obj->coordinates[count($obj->coordinates) - 1][0]))->results[0]->components;

        $from->embedded_type = "administrative_region";
        $from->name = $this->getPlaceName($fromPlace);

        $to->embedded_type = "administrative_region";
        $to->name =  $this->getPlaceName($toPlace);

        $geojson->type = "LineString";
        $geojson->coordinates = $obj->coordinates;

        // Display informations
        $display_informations->code = isset ($obj->properties->description) ? $obj->properties->description : "";


        // SECTION
        $section->from = $from;
        $section->departure_date_time = date('c');
        $section->arrival_date_time = date_format(date_create(date_default_timezone_get())->add(date_interval_create_from_date_string($obj->properties->traveltime."seconds")), 'c');
        $section->display_informations = $display_informations;
        $section->to = $to;
        $section->geojson = $geojson;
        $section->duration = intval($obj->properties->traveltime);
        $section->distance = intval($obj->properties->distance);
        $section->type = "street_network";
        switch($this->vehicle)
        {
            case 'motorcar' :
                $section->icon = 'img/icons/car.png';
                break;
            case 'foot' :
                $section->icon = 'img/icons/walking.png';
                break;
            case 'bicycle' :
                $section->icon = 'img/icons/bike.png';
                break;
            default :
                $section->icon = 'img/icons/car.png';
                break;
        }
        $section->mode = $this->vehicle;

        array_push($sections, $section);

        array_push($journeys, array("sections" => $sections));

        return $journeys[0];
    }

    private function getPlaceName($place)
    {
        if(isset($place->city)) {
            return $place->city;
        } else if(isset($place->village)) {
            return $place->village;
        } else if(isset($place->bus_stop)) {
            return $place->bus_stop;
        } else if(isset($place->town)) {
            return $place->town;
        } else if(isset($place->county)) {
            return $place->county;
        }
        else {
            return $place->{$place->_type};
        }
    }

    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;
    }

}
