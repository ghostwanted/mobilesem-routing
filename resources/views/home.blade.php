@extends('layouts.master')

@section('title', 'Home')

@section('style')
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet"/>
    <style type="text/css">
        #map {
            height: 500px;
            width: 600px;
        }
    </style>
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="row">

        <div class="col-md-6">

            <div id="alert" class="alert alert-danger" role="alert" style="display: none;">
                <i class="fa fa-exclamation-triangle"></i>
                <span class="sr-only">Errors</span>
                Please be sure that from and to addresses are selected
            </div>

            <form id="form" action="{{URL::route('route.show')}}" method="GET">
                <!-- Search bar From -->
                <div class="row">
                    <div class="col-md-12">
                        <table>
                            <tr>
                                <td>
                                    <h2>From</h2>
                                </td>
                                <td>
                                    <img src="img/marker-azure.png" width="32" length="32">
                                </td>
                            </tr>
                        </table>
                        <div id="search-input-from">
                            <div id="from-group" class="input-group col-md-12">
                                <input type="text" class="form-control" placeholder="e.g. Hamoir"  id="fromSearch">
                                <input type="hidden" name="flat" id="fromLat">
                                <input type="hidden" name="flon" id="fromLon">
                      <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" disabled>
                              <i class="fa fa-search"></i>
                          </button>
                      </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Search bar To -->
                <div class="row">
                    <div class="col-md-12">
                        <table>
                            <tr>
                                <td>
                                    <h2>To</h2>
                                </td>
                                <td>
                                    <img src="img/marker-pink.png" width="32" length="32">
                                </td>
                            </tr>
                        </table>
                        <div id="search-input-to">
                            <div id="to-group" class="input-group col-md-12">
                                <input type="text" class="form-control" placeholder="e.g. Liège" id="toSearch">
                                <input type="hidden" name="tlat" id="toLat">
                                <input type="hidden" name="tlon" id="toLon">
                      <span class="input-group-btn">
                          <button class="btn btn-primary" type="button" disabled>
                              <i class="glyphicon glyphicon-search"></i>
                          </button>
                      </span>
                            </div>
                        </div>
                    </div>
                </div>

                <br />
                <!-- Checkbox -->
                <h5>Public transport</h5>
                <div class="checkbox">
                    <label>
                        <input value="train" class="vCheck" type="checkbox" checked> <img src="img/icons/train.png" width="14" length="14" alt="train"/> Train
                    </label>
                    <label>
                        <input value="bus" class="vCheck" type="checkbox" checked> <img src="img/icons/bus.png" width="18" length="18" alt="bus"/> Bus
                    </label>
                    <label>
                        <input value="subway" class="vCheck" type="checkbox" checked> <img src="img/icons/metro.png" width="22" length="22" alt="subway"/> Metro
                    </label>
                    <label>
                        <input value="tramway" class="vCheck" type="checkbox" checked> <img src="img/icons/tramway.png" width="16" length="16" alt="tramway"/> Tramway
                    </label>
                </div>
                <h5>Shared transport</h5>
                <div class="checkbox">
                    <label>
                        <input value="carpool" class="vCheck" type="checkbox" > <img src="img/icons/carpool.png" width="20" length="20" alt="carpool"/> Carpool
                    </label>
                    <label>
                        <input value="carsharing" class="vCheck" type="checkbox" > <img src="img/icons/carsharing.png" width="22" length="22" alt="carsharing"/> Carsharing
                    </label>
                    <label>
                        <input value="uber" class="vCheck" type="checkbox" > <img src="img/icons/uber.png" width="18" length="18" alt="uber"/> Uber
                    </label>
                    <label>
                        <input value="bicyclesharing" class="vCheck" type="checkbox" > <img src="img/icons/bicycle-sharing.png" width="22" length="22" alt="bicycle-sharing"/> Bicycle-sharing
                    </label>
                </div>
                <h5>Private transport</h5>
                <div class="checkbox">
                    <label>
                        <input value="motorcar" class="vCheck" type="checkbox" checked> <img src="img/icons/car.png" width="18" length="18" alt="car" /> Car / Motorcycle
                    </label>
                    <label>
                        <input value="bicycle" class="vCheck" type="checkbox"> <img src="img/icons/bike.png" width="22" length="22" alt="bicycle" /> Bicycle
                    </label>
                    <label>
                        <input value="foot" class="vCheck" type="checkbox"> <img src="img/icons/walking.png" width="14" length="14" alt="walking" /> Walking
                    </label>
                </div>
                <input type="hidden" id="vehicles" name="v" value="train,bus,subway,tramway,motorcar">
                <br />
                <div class="row">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary btn-block">Find the best journey <i class="fa fa-smile-o"></i></button>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6">
            <div id="map"></div>
        </div>
    </div>

@endsection

@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
    <script src="{{URL::to('/')}}/js/global.js"></script>
    <script src="{{URL::to('/')}}/js/utils.js"></script>
    <script src="{{URL::to('/')}}/js/mapview.js"></script>
    <script src="{{URL::to('/')}}/js/geocoding.js"></script>
    <script src="{{URL::to('/')}}/js/home.js"></script>
@endsection