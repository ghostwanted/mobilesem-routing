@extends('layouts.master')

@section('title', 'Results')

@section('style')
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
    <style type="text/css">
        #map {
            height: 500px;
            width: 600px;
        }
        .btn-info {
            background:url('img/icons/info.png') no-repeat;
            height: 26px;
            width: 26px;
            cursor:pointer;
            border: none;

        }

    </style>
@endsection

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="row">

        <div class="col-md-6" id="results-col">
            <br />
            <div id="title"></div>
            <br />
            <div id="results-panel" class="panel panel-primary">
                <div id="results-body" class="panel-body">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <h3 class="text-inverse"><i class="fa fa-refresh fa-spin"></i> Loading</h3>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6">
            <div id="map"></div>
        </div>
    </div>
@endsection

@section('script')
    <script src="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js"></script>
    <script src="{{URL::to('/')}}/js/global.js"></script>
    <script src="{{URL::to('/')}}/js/utils.js"></script>
    <script src="{{URL::to('/')}}/js/mapview.js"></script>
    <script src="{{URL::to('/')}}/js/route.js"></script>
@endsection